package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Project;

import java.util.List;
import java.util.Comparator;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project add(Project project);

    void remove(Project project);

    Project removeById(String id);

    Project removeByName(String Name);

    Project removeByIndex(Integer index);

    void clear();

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

}
