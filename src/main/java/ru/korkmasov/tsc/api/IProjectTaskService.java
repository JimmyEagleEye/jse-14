package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.Project;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(final String projectId);

    Task bindTaskById(final String taskId, final String projectId);

    Task unbindTaskById(final String taskId);

    void removeTaskFromProjectById(String id);

    Project removeProjectById(final String projectId);

    Project removeProjectByIndex(final Integer index);

    Project removeProjectByName(final String name);

}
