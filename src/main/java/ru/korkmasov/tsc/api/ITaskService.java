package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;

import java.util.List;
import java.util.Comparator;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task add(String name, String description);

    Task add(Task task);

    void remove(Task task);

    Task removeById(String id);

    Task removeByName(String Name);

    Task removeByIndex(Integer index);

    void clear();

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

}
