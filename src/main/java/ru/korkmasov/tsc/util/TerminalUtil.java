package ru.korkmasov.tsc.util;

import java.util.Scanner;

public class TerminalUtil {

    static Scanner SCANNER = new Scanner(System.in);

    public static String nextLine(){
        return SCANNER.nextLine();
    }

    public static Integer nextNumber(){
        final String value = SCANNER.nextLine();
        return Integer.parseInt(value);
    }

}
