package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.ITaskRepository;
import ru.korkmasov.tsc.api.ITaskService;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.enumerated.Status;

import java.util.List;
import java.util.Comparator;
import java.util.Date;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        taskRepository.add(task);
        return task;
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task updateById(final String id, final String name, final String description){
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task=taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description){
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task=taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Task task = taskRepository.findByIndex(index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByName(name);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Task task = taskRepository.findByIndex(index);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByName(name);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

}
