package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.IProjectTaskService;
import ru.korkmasov.tsc.api.ITaskRepository;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.api.IProjectRepository;
import ru.korkmasov.tsc.model.Project;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    public Project removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public List<Task> findTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskById(final String taskId, final String projectId) {
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.bindTaskToProjectById(taskId, projectId);
    }

    @Override
    public Task unbindTaskById(final String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public void removeTaskFromProjectById(String id) {
        if (id == null || id.isEmpty()) return;
        taskRepository.removeAllTaskByProjectId(id);
    }

    @Override
    public Project removeProjectByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        String projectId = projectRepository.findByIndex(index).getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Project removeProjectByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        String projectId = projectRepository.findByName(name).getId();
        taskRepository.removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

}
