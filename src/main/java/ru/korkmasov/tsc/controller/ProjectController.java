package ru.korkmasov.tsc.controller;

import ru.korkmasov.tsc.api.IProjectController;
import ru.korkmasov.tsc.api.IProjectService;
import ru.korkmasov.tsc.api.IProjectTaskService;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showById(){
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project==null){
            System.out.println("Incorrect values");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByIndex(){
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber()-1;
        final Project project = projectService.findByIndex(index);
        if (project==null){
            System.out.println("Incorrect values");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByName(){
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project==null){
            System.out.println("Incorrect values");
            return;
        }
        showProject(project);
    }

    private void showProject (Project project){
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public Project add(String name, String description) {
        return null;
    }

    @Override
    public void removeById(){
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project==null) System.out.println("Incorrect values");
        projectTaskService.removeTaskFromProjectById(id);
    }

    @Override
    public void removeByIndex(){
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber()-1;
        final Project project = projectService.removeByIndex(index);
        if (project == null)
        {
            System.out.println("Incorrect values");
            return;
        }
        projectTaskService.removeTaskFromProjectById(project.getId());
    }

    @Override
    public void removeByName(){
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeById(name);
        if (project == null)
        {
            System.out.println("Incorrect values");
            return;
        }
        projectTaskService.removeTaskFromProjectById(project.getId());
    }

    @Override
    public void updateByIndex(){
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber()-1;
        final Project project = projectService.findByIndex(index);
        if (project == null){
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateByIndex(index, name, description);
        if (projectUpdated==null) System.out.println("Incorrect values");
    }

    @Override
    public void updateById(){
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null){
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateById(id, name, description);
        if (projectUpdated==null) System.out.println("Incorrect values");
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void startByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void finishById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void finishByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void finishByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
